<?php defined('ABSPATH') || exit; ?>

<div class="ns-no-more">
    <i class="far fa-frown-open"></i>
    <span><?php esc_html_e('There are no more articles', 'ns'); ?></span>
</div>