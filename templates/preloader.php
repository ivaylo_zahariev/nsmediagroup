<?php defined('ABSPATH') || exit; ?>

<div class="ns-preloader">
    <i class="fas fa-sync fa-spin"></i>
    <span><?php esc_html_e('Loading next article', 'ns'); ?></span>
</div>