<?php defined('ABSPATH') || exit; ?>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-106066163-6"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-106066163-6');
</script>