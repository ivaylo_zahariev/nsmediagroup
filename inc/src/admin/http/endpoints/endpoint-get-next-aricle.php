<?php

/*
 * handle get next article ajax request
 *
 */
namespace Ns\Inc\Src\Admin\Http\Endpoints;

use \Ns\Inc\Src\Request\Request;

if ( ! defined('ABSPATH') ) {
	exit;
}

class Endpoint_Get_Next_Aricle extends Endpoint {

	public $action = 'ns_get_next_aricle';

    public function action() {

		// sleep for .3 seconds, so you can see there is a preloader
		usleep( 300000 );

		$request = Request::instance();

		// if there is no post_id, bail
		if( $request->is_empty('post_id') or $request->is_empty('agent') ) {
			return;
		}

		// security check
		if ( ! wp_verify_nonce( $request->get('security'), 'ns-ajax-nonce' ) ) {
			return;
		}

		// set the current post globally, so we can use `get_previous_post` method
		global $post;
		$post = get_post( $request->get('post_id') );
		setup_postdata( $post );

		// get previous post
		$previous_post = get_previous_post();

		if( $previous_post ) {

			// extract logged in cookie from current cookies
			$cookies = [];
			foreach( $_COOKIE as $cookie_name => $cookie_value ) {
			    if( strpos( $cookie_name, 'wordpress_logged_in') !== false ) {

					// create the cookie
					$cookie = new \WP_Http_Cookie( $cookie_name );
					$cookie->name = Ns()->sanitize( $cookie_name );
					$cookie->value = Ns()->sanitize( $cookie_value );
					$cookies[] = $cookie;

			        break;

			    }
			}

			// get previous post content remotely
			$response = wp_remote_get( get_permalink( $previous_post ), [
				'method' => 'GET',
				'user-agent' => $request->get('agent'),
			    'cookies' => $cookies,
			]);

			// make sure there is no errors
			if ( ! is_wp_error( $response ) ) {

				// output `success`
				wp_send_json([
					'success' => true,
					'post_id' => $previous_post->ID,
					'url' => get_permalink( $previous_post->ID ),
					'title' => get_the_title( $previous_post->ID ),
					'html' => wp_remote_retrieve_body( $response )
				]);

			}

		}

		wp_reset_postdata();

		// output `no more articles`
		wp_send_json([
			'success' => false,
			'nope' => Ns()->get_template('no-more-articles')
		]);

	}

}
