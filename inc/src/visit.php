<?php

/*
 * track and store user visits
 *
 */
namespace Ns\Inc\Src;

class Visit {

    // add new visit
    public function add() {

        global $wpdb;

        if( $_SERVER['REQUEST_METHOD'] !== 'GET' ) {
            return;
        }

        $wpdb->insert( $wpdb->prefix . 'ns_visits', [
            'ip' => $this->get_ip(),
            'browser' => $this->get_browser(),
            'url' => $this->get_url(),
            'created_at' => date('Y-m-d H:i:s'),
        ]);

    }

    // get user ip address if available in $_SERVER
    public function get_ip() {

		$ip = null;

		$keys = [
			'HTTP_CLIENT_IP',
			'HTTP_X_FORWARDED_FOR',
			'HTTP_X_FORWARDED',
			'HTTP_FORWARDED_FOR',
			'HTTP_FORWARDED',
			'REMOTE_ADDR',
		];

		foreach( $keys as $key ) {
			if( ! empty( $_SERVER[ $key ] ) ) {
				$ip = trim( $_SERVER[ $key ] );
				break;
			}
		}

		if( $ip ) {

			// make sure it's a valid ip address
			if( filter_var( $ip, FILTER_VALIDATE_IP ) ) {
				return $ip;
			}

			// sometimes multiple ip's are returned in comma-separated format
			$ips = explode( ',', $ip );
			$first_ip = trim( $ips[0] );
			if( filter_var( $first_ip, FILTER_VALIDATE_IP ) ) {
				return $first_ip;
			}

		}

		return null;
	}

    // extract the browser from user agent
    public function get_browser() {

		$user_agent = $_SERVER['HTTP_USER_AGENT'];
		$browser = null;
		$browser_array  = [
			'/msie/i' => 'ie',
			'/firefox/i' => 'firefox',
			'/safari/i' => 'safari',
			'/chrome/i' => 'chrome',
			'/edge/i' => 'edge',
			'/opera/i' => 'opera',
			'/mobile/i' => 'handheld'
		];

		foreach( $browser_array as $regex => $value ) {
			if( preg_match( $regex, $user_agent ) ) {
				$browser = $value;
			}
		}

		return $browser;

	}

    // extract the url
    public function get_url() {
        return $_SERVER['REQUEST_URI'];
    }

}
