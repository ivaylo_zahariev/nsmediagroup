<?php

/*
 * main plugin setup class
 *
 */
namespace Ns\Inc\Src;

use \Ns\Inc\Src\Views;

class Setup {

    use \Ns\Inc\Src\Traits\Singleton;

    function __construct() {

        // scripts
        add_action( 'wp_enqueue_scripts', [ $this, 'enqueue_scripts' ] );

        // add google analytics
        add_action( 'wp_head', [ $this, 'add_analytics' ] );

        // handle user page visits
        add_action( 'wp_head', [ $this, 'visits' ] );

        // article after content, inject preloader
        add_action( 'ns/preloader', [ $this, 'ns_preloader_template' ] );

    }

    // set global vars to be passed to js
    public function get_vars() {

        global $md_explore;

        $vars = [
            'admin_ajax' => admin_url('admin-ajax.php'),
            'nonce' => wp_create_nonce('ns-ajax-nonce'),
            'site_url' => site_url('/'),
            'uri_assets' => NS_URI,
            'localize' => [
                'example' => esc_html__( 'Example', 'master-directory' ),
            ],
            'agent' => $_SERVER['HTTP_USER_AGENT'],
            'ga_measurement_id' => 'UA-106066163-6'
        ];

        if( is_single() and is_main_query() and get_post_type() == 'post' ) {
            $vars['post_id'] = get_the_ID();
        }

        return $vars;
    }

    // enqueue scripts
    public function enqueue_scripts() {

        // font awesome
        wp_enqueue_style( 'font-awesome', NS_URI . 'assets/dist/fonts/font-awesome/css/all.min.css' );

        /*
         * main js
         *
         */
        wp_register_script( 'md-main', NS_URI . 'assets/dist/js/main.js', ['jquery'/*, 'gsap'*/], NS_VERSION, true );
        wp_localize_script( 'md-main', 'ns_vars', $this->get_vars() );
        wp_enqueue_script( 'md-main' );

        /*
         * main css
         *
         */
        wp_enqueue_style( 'md-style', NS_URI . 'assets/dist/css/main.css' );

    }

    // inject analytics template
    public function add_analytics() {
        Ns()->the_template('analytics');
    }

    // inject preloader after article content
    public function ns_preloader_template() {
        if( is_single() and in_the_loop() and is_main_query() and get_post_type() == 'post' ) {
            Ns()->the_template('preloader');
        }
    }

    // add a new visit
    public function visits() {

        $visit = new Visit();
        $visit->add();

    }

}
