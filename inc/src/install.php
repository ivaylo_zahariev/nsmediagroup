<?php

/*
 * fire after plugin installation
 *
 */
namespace Ns\Inc\Src;

use \Ns\Inc\Src\Traits\Singleton;

class Install {

    use Singleton;

    // database version
    public $version = '1.0';

    function __construct() {

        // hook to fire after plugin installation
        register_activation_hook( NS_PLUGIN, [ $this, 'install' ] );

    }

    // handle database
    public function install() {

        $this->create_table_visits();
        $this->update_db_version();

    }

    // create tables
    public function create_table_visits() {

        global $wpdb;

        $table_name = $wpdb->prefix . 'ns_visits';
        $charset_collate = $wpdb->get_charset_collate();

        $sql = "
            CREATE TABLE $table_name (
                id mediumint(9) NOT NULL AUTO_INCREMENT,
                ip varchar(32) NULL,
                browser varchar(64) NULL,
                url text null,
                created_at datetime DEFAULT CURRENT_TIMESTAMP NOT NULL,
                PRIMARY KEY (id)
            ) $charset_collate;
        ";

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta( $sql );

    }

    // insert our db version
    public function update_db_version() {
        update_option( 'ns_db_version', $this->version );
    }

}
