'use strict'

import 'waypoints/lib/noframework.waypoints.min.js';

window.$ = window.jQuery
window.Ns = window.Ns || {}

/*
 * main scripts
 *
 */
class Ns {

	constructor() {

		this.$body = $('body')
		this.waypoint_bottom = this.waypoint_top = null

		$(document).ready(() => this.ready())

	}

	// on document ready
	ready() {

		this.load_next()

	}

	// listen next article in viewport using `waypoint`
	load_next() {

		let $e = $('.ns-preloader')

		if( ! $e.length ) {
			return;
		}

		// scrolling to bottom
		this.waypoint_bottom = new Waypoint({
			element: $e.get(0),
			handler: ( dir ) => {
				this.handle_waypoint_bottom( dir )
			},
			offset: '100%'
		})

		// scrolling to top
		this.waypoint_top = new Waypoint({
			element: $e.get(0),
			handler: ( dir ) => {
				this.handle_waypoint_top( dir )
			},
			offset: '0%'
		})

	}

	// in we are in the right place, make sure we are good to go
	handle_waypoint_bottom( dir ) {

		let $e = $('.ns-preloader')

		let bottom = $e.position().top + $e.outerHeight()
		if( bottom < $(window).scrollTop() ) {
			return;
		}

		if( dir === 'down' ) {
			this.waypoint_action()
		}

	}

	// in we are in the right place, make sure we are good to go
	handle_waypoint_top( dir ) {

		if( dir === 'up' ) {
			this.waypoint_action()
		}

	}

	// yep, we can call the new article and destroy the waypoint
	waypoint_action() {

		this.get_next_article()

		this.waypoint_bottom.destroy()
		this.waypoint_top.destroy()

	}

	// send the ajax call to get the next article
	get_next_article() {

		$.ajax({
            type: 'post',
            dataType: 'json',
            url: window.ns_vars.admin_ajax,
            data: {
                action: 'ns_get_next_aricle',
				security: window.ns_vars.nonce,
				agent: window.ns_vars.agent,
				post_id: window.ns_vars.post_id
            },
			beforeSend: () => {

				this.$body.addClass('ns-ajaxing')

			},
			complete: () => {

				this.$body.removeClass('ns-ajaxing')

			},
			success: ( response ) => {

				// success
				if( response.success ) {

					// remove the preloader
					$('.ns-preloader').remove()
					// append the new article content
					$('.post:last').after( $( response.html ).find('.post') )
					// change the url
					history.pushState( { page: 'article' }, response.title, response.url )
					// change the title
					document.title = response.title
					// set the new post id
					window.ns_vars.post_id = response.post_id
					// push pageview to google analytics
					if ( 'gtag' in window ) {
						window.gtag( 'config', window.ns_vars.ga_measurement_id, {
							'page_path': response.url
						})
					}
					// re-call waypoints
					this.load_next()

				}
				// no more articles
				else{

					// append `no more articles` message
					$('.post:last').append( response.nope )

				}

			}
        })

	}

}

new Ns()
